"use strict";

module.exports = {
  up: function(migration, DataTypes, done) {
    // add altering commands here, calling 'done' when finished
	migration.createTable(
			'photos',
			{
				id: {
					type: DataTypes.INTEGER,
					primaryKey: true,
					autoIncrement: true
				},
				createdAt: {
					type: DataTypes.DATE
				},
				updatedAt: {
					type: DataTypes.DATE
				},
	// https://developers.google.com/maps/articles/phpsqlsearch_v3?csw=1
				latitude: DataTypes.FLOAT(10, 6),
				longitude: DataTypes.FLOAT(10,6),
				filepath: DataTypes.STRING, // default 255
				filesize: DataTypes.INTEGER,
				description: DataTypes.STRING, 
				camera: DataTypes.STRING,
				fstop: DataTypes.INTEGER,
				iso: DataTypes.INTEGER,
				upvote: DataTypes.INTEGER,
				downvote: DataTypes.INTEGER,
				userID: DataTypes.INTEGER
			},
			{
				engine: 'MYISAM',
				charset: 'latin1'
			}
	);
				
    done();
  },

  down: function(migration, DataTypes, done) {
    // add reverting commands here, calling 'done' when finished
	migration.dropTable('photos');
    done();
  }
};

var express = require('express');
var fs = require('fs');
var router = express.Router();
var models = require('../models');
var rmdir = require('rimraf');
var utils = require('utils');

/* GET home page. */
router.get('/', function(req, res) {
  //res.render('index', { title: 'LivingSnap' });
  res.sendfile('/pages/globe.html', {root: './public'});
});

router.get('/upload', function(req, res) {
	res.render('upload', {title: 'Upload' });
});

router.post('/upload', function(req, res) {
	var path = __dirname + "/public/uploads";
	var filename = "";
	var red = models.photo.storePhoto(req.files.photo, function(absPath){
		rmdir(absPath, function(error){
			if(error) console.log("error deleting " + absPath +
				": " + error.message);
		});
	});	
	
	res.render('view', {photo: red});
});

router.get('/photos', function(req, res) {
	models.photo.findAll().success(function(photos){
		res.render('photos', {photos: photos, curTime: utils.mysqlDate()});
	});	
});

router.get('/photos/:lastTime', function(req, res) {
	models.photo.findAll({where: "createdAt > '" + req.params.lastTime+ "'"}).success(function(photos){
		res.render('photos', {photos: photos, curTime: utils.mysqlDate()});
	});
});

router.get('/getStats', function(req, res){
	var statStr = models.photo.getStats(function(statStr){
		res.render('stats', {stats: statStr});		
	});
});
	
module.exports = router;

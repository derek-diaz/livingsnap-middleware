// LIVINGSNAP 2014
// DEREK DIAZ CORREA 
// script.js
// This file contains the ajax call and the main earth method.

var earth; 
var timeStamp;

$(document).ready(function() {

  $.ajax({
    url: "/photos",
    type: "GET",
    cache: false,
    crossDomain: true,
    dataType: "html",
    success: function (data) {
		var contentData = $(data).find("ul"); //$(data).find('ul');
        //Get TimeStamp
        var requestTime = $(data).find('currentTime');          
        timeStamp = requestTime.prevObject[0].innerHTML;
		//Parse Data
		for (var i = 0; i < contentData.length; i++){
			var data = $(contentData[i]).find("li");
			var snapObj = new snap(data);
			var marker = WE.marker([snapObj.latitude, snapObj.longitude]).addTo(earth);
        	marker.bindPopup("<img class='snapPhoto' src='" + snapObj.filepath+ "'/>", {maxWidth: 150, closeButton: false});
		}
        //jsonCallback = data.Result;
    },
    error: function (e) {
        alert(e.responseText);
    }
});
/*
  $.ajax({
    url: "/getStats",
    type: "GET",
    cache: false,
    crossDomain: true,
    dataType: "html",
    success: function (data) {
		var contentData = $(data).find("body"); //$(data).find('ul');
	    var parsed = data.split("~");
		var txt = parsed[0].replace('<','');
		txt = txt.trim();
		$("#statsTotal").text(txt);
		var txt = parsed[1];
		$("#statsDistance").text(txt);
		var txt = parsed[2];
		$("#statsCamera").text(txt);
		var txt = parsed[3];
		txt = txt.substring(0, txt.indexOf('<'));
		txt = txt.replace('>','');
		txt = txt.trim();
		$("#statsTags").text(txt);
	//	parsed = parsed.split(":")
	//parsed = parsed[1].trim();
    },
    error: function (e) {
        alert(e.responseText);
    }
});
 */   
 window.setInterval(function(){
      $.ajax({
    url: "/photos/" + timeStamp,
    type: "GET",
    cache: true,
    data: "",
    crossDomain: true,
    dataType: "html",
    success: function (data) {
		var contentData = $(data).find("ul");
        //Get TimeStamp
        var requestTime = $(data).find('currentTime');       
        timeStamp = requestTime.prevObject[0].innerHTML;
		//Parse Data
		for (var i = 0; i < contentData.length; i++){
			var data = $(contentData[i]).find("li");
			var snapObj = new snap(data);
			var marker = WE.marker([snapObj.latitude, snapObj.longitude]).addTo(earth);
        	marker.bindPopup("<img class='snapPhoto' src='" + snapObj.filepath+ "'/>", {maxWidth: 150, closeButton: false});
		}
        //jsonCallback = data.Result;
    },
    error: function (e) {
        alert(e.responseText);
    }
});

}, 5000);


});




function initialize() {
        earth = new WE.map('globe');
        WE.tileLayer('http://otile{s}.mqcdn.com/tiles/1.0.0/sat/{z}/{x}/{y}.jpg', {
            subdomains: '1234',
            attribution: 'Tiles Courtesy of MapQuest'
        }).addTo(earth);
}

// LIVINGSNAP 2014
// DEREK DIAZ CORREA 
// snap.js
// This a custom object for each snap.

function snap(data){
	this.id = parser(data[0]);
	this.latitude = (parseFloat(parser(data[1])) + parseFloat((Math.random() * (0.00000 - 0.00009) + 0.00009).toFixed(5)));
	this.longitude = (parseFloat(parser(data[2])) + parseFloat((Math.random() * (0.00000 - 0.00009) + 0.00009).toFixed(5)));
	this.filepath = parser(data[3]);
	this.filesize = parser(data[4]);
	this.description = parser(data[5]);
	this.camera = parser(data[6]);
	this.fstop = parser(data[7]);
	this.iso = parser(data[8]);
	this.upvote = parser(data[9]);
	this.downvote = parser(data[10]);
	this.userID = parser(data[11]);
}

function parser(data){
	var parsed = data.outerText;
	parsed = parsed.split(":")
	parsed = parsed[1].trim();
	return parsed;
}



"use strict";
// this model will describe a photo and contain the necessary
// functions for interaction with hadoop and the database.
var exif = require('exif').ExifImage;
var fs = require('fs');
var utils = require('utils');
var path = require('path');
var rmdir = require('rimraf');
var WebHDFS = require('webhdfs');
var hdfs = WebHDFS.createClient();
var client = require('scp2');

module.exports = function(sequelize, DataTypes){
	var Photo = sequelize.define('photo', {
		latitude:	DataTypes.FLOAT(10, 6),
		longitude:	DataTypes.FLOAT(10, 6),
		filepath:	DataTypes.STRING,
		filesize:	DataTypes.INTEGER,
		description:	DataTypes.STRING,
		camera:		DataTypes.STRING,
		fstop:		DataTypes.INTEGER,
		iso:		DataTypes.INTEGER,
		upvote:		DataTypes.INTEGER,
		downvote:	DataTypes.INTEGER,
		userID:		DataTypes.INTEGER
	}, {
		classMethods: {
			/**
			 * Parses string of metadata into usable
			 * array
			 **/
			parseMetadata: function(jpeg, callback){
				try {
					new exif({image : jpeg }, function (err, exifData){
						if(err)
							console.log("Error parsing metadata for " + jpeg + " : " + err.message);
						else{
							var metadata = [];
							var lat = JSON.stringify(exifData.gps.GPSLatitude)
								.replace("[", "").replace("]", "").split(',');
							var lon = JSON.stringify(exifData.gps.GPSLongitude)
								.replace("[", "").replace("]", "").split(',');
							metadata["latitude"] = utils
								.gpsToDecimal(lat[0], lat[1], lat[2], JSON.stringify(exifData.gps.GPSLatitudeRef).charAt(1));
							metadata["longitude"] = utils
								.gpsToDecimal(lon[0], lon[1], lon[2], JSON.stringify(exifData.gps.GPSLongitudeRef).charAt(1));
							metadata["fstop"] = exifData.exif.FNumber;
							metadata["iso"] = exifData.exif.ISO;
							metadata["camera"] = exifData.image.Make;
							console.log(exifData);
							callback(metadata);
						}
					});
				}catch(error){
					console.log("Error creating exif object: " + error.message);
				}
			},
			/**
			 * Call this function insetad of photo.build().
			 * updates hadoop textfile, then inserts photo.
			 * Pass file object from req.files
			 **/
			storePhoto: function(file, callback){
				var folder = path.dirname(path.dirname(file.file));	
				var root = path.dirname(path.dirname(process.mainModule.filename));
				var absPath = root + "/" + folder;
				var fileName = path.basename(file.file);
				var savePath = utils.createFilename(root + "/public/uploads", fileName);
				// move file to permanent folder
				var readStream = fs.createReadStream(absPath + "/photo/" + fileName);
				readStream.once('readable', function(){

					readStream.pipe(fs.createWriteStream(savePath));
				});
				readStream.once('end', function(){
					var stats = fs.statSync(savePath);
					console.log('saved file: ' + savePath);
					Photo.parseMetadata(savePath, function(metadata){
						// write to database (use array from parseMetadata()
						var photo = Photo.build({
						filepath: "uploads/" + path.basename(savePath),
						iso: metadata["iso"],
						fstop: metadata["fstop"],
						latitude: metadata["latitude"],
						longitude: metadata["longitude"],
						camera: metadata["camera"],
						});
						photo.save();	
						//Photo.hadoopUpdate(metadata, fileName, savePath, "test description", stats["size"]);
						// stream is done, delete the old file
						callback(absPath);
					}); 
				});
				return "uploads/" + path.basename(savePath);
			},
			/**
			 * Reads stat file from hadoop and returns it as a string.
			 **/
			getStats: function(callback){
				var remoteFile = "group11:group11@10.242.144.220:/opt/www/livingsnap/stats.txt";
				var localFile = "/opt/www/livingsnap/stats.txt";
				client.scp(remoteFile, localFile, function(err){
					if(err) console.info("Error retrieving stats: " + err.message);
					else{
					 fs.readFile(localFile, 'utf8', function(error,data){
						if(error) console.info("Error reading local stat file: " + error.message);
						else
				 			callback(data);		 
					 });
					}
				});
			},
			/**
			 * Hadoop textfile udpater
			 **/
			hadoopUpdate: function(metadata, fileName, savePath, description, fileSize){
				var hadoopStr = metadata['latitude'] + "," + metadata['longitude'] + 
					"," + fileName + "," + savePath + "," + description + "," + fileSize +
					"," + metadata["camera"] + "," + "MODELGOESHERE" + "," + metadata["fstop"] +
					"," + "TAGSGOHERE" + "," + "0" + "," + "0\n";
				console.info("***Hadoop String: " + hadoopStr);
				var fileName = "/home/group11/livingsnap/imagedata/data1.input";
				fs.appendFile(fileName, hadoopStr, function(err){
					if(err)
						console.info("ERROR: append to hadoop file failed - " + err.message);
					client.scp(fileName, 'group11:group11@10.242.144.220:' + fileName, function(error){
						if(error) 
							console.info("ERROR: scp hadoop file failed - " + error.message);	
					});
				});

			},
			/**
			 * Returns array of photos that are within a square 
			 * on the map
			 **/
			getPhotosInArea: function(minLat, maxLat, minLong, maxLong){
			}
		},
		instanceMethods: {
			/**
			 * put voting score logic here
			 **/
			voteRatio: function(){
				var ratio = this.upvote / this.downvote;
				return ratio;
			}
		}
		
	});
	return Photo;
}
